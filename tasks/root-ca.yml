---
- name: set ca root dir
  set_fact:
    ca_root_dir: "{{ca_dir}}/{{ca_name}}"

- name: create directorys for our ca
  file:
    path: "{{item}}"
    owner: root
    group: root
    mode: 0700
    state: directory
  with_items:
  - "{{ca_root_dir}}"
  - "{{ca_root_dir}}/private"
  - "{{ca_root_dir}}/db"
  - "{{ca_root_dir}}/etc"
  - "{{ca_root_dir}}/certs"
  - "{{ca_root_dir}}/ca"
  - "{{ca_root_dir}}/crl"

- name: make sure db files exists
  file:
    path: "{{item}}"
    owner: root
    group: root
    mode: 0600
    state: touch
  with_items:
  - "{{ca_root_dir}}/db/root-ca.db"
  - "{{ca_root_dir}}/db/root-ca.db.attr"

- name: Create serial file
  copy:
    dest: "{{item}}"
    content: "01"
    force: no
  with_items:
  - "{{ca_root_dir}}/db/root-ca.crt.srl"
  - "{{ca_root_dir}}/db/root-ca.crl.srl"

- name: Copy ca config template
  template:
    dest: "{{ca_root_dir}}/etc/{{ca_name}}.conf"
    src: "{{ca_name}}.conf.j2"

- name: Check if we need key
  stat:
    path: "{{ca_root_dir}}/private/root-ca.key"
  register: r_ca_key_stat

- name: Create key if not exists
  command: |
    openssl req -new \
    -config {{ca_root_dir}}/etc/root-ca.conf \
    -out {{ca_root_dir}}/ca/root-ca.csr \
    -keyout {{ca_root_dir}}/private/root-ca.key \
    -passout {{'pass:' + ca_password | quote}}
  no_log: true
  when:
  - not r_ca_key_stat.stat.exists

- name: check if key needs a certificate
  stat:
    path: "{{ca_root_dir}}/ca/root-ca.crt"
  register: r_ca_cert_stat

- name: Create certificate for ca
  command: |
    openssl ca -selfsign -batch \
    -config {{ca_root_dir}}/etc/root-ca.conf \
    -in {{ca_root_dir}}/ca/root-ca.csr \
    -out {{ca_root_dir}}/ca/root-ca.crt \
    -extensions root_ca_ext \
    -enddate {{ca_end_date}} \
    -passin {{'pass:' + ca_password | quote}}
  no_log: true

- name: check if revocation list exists
  stat:
    path: "{{ca_root_dir}}/crl/root-ca.crl"
  register: r_ca_revocation_stat
  when:
  - ca_root_url is defined

- name: create empty revocation list
  command: |
    openssl ca -gencrl \
    -config {{ca_root_dir}}/etc/root-ca.conf \
    -out {{ca_root_dir}}/crl/root-ca.crl \
    -passin {{'pass:' + ca_password | quote}}
  when:
  - ca_root_url is defined
  - not r_ca_revocation_stat.stat.exists
  no_log: true
